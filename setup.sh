echo ""
cat misc/title.txt

if [[ $(openstack --version) ]]; then
    echo ""
else
    echo "installing the OpenStack CLI..."
	if ! [[ $(pip --version) ]]; then
		sudo apt --yes install python3-pip
	fi
	pip install python-openstackclient
fi

# Set the color variable
green='\033[0;32m'
red='\e[1;31m'
# Clear the color after that
clear='\033[0m'

if [ -a user_data.txt ]
then 
 rm user_data.txt
fi

echo ""
echo "===================================================================="
ls
echo ""
export fileconfig=$(awk -F "=" '/fileconfig/ {print $2}' config.ini)
echo -e "Bash configuration file: ${red}$fileconfig${clear}";
source $(echo "${fileconfig}")

echo ""
echo "===================================================================="
TF_VAR_username=$(sed -nr "/^\[INSTANCE\]/ { :l /^username[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_username=${TF_VAR_username//[[:blank:]]/}
echo -e "Name for the user:  ${red}$TF_VAR_username${clear}";

echo ""
echo "===================================================================="
TF_VAR_password=$(sed -nr "/^\[INSTANCE\]/ { :l /^password[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_password=${TF_VAR_password//[[:blank:]]/}
echo -e -n "Password for the instance created:  ${red}$TF_VAR_password${clear}";


echo ""
echo "===================================================================="
TF_VAR_hostname=$(sed -nr "/^\[INSTANCE\]/ { :l /^hostname[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_hostname=${TF_VAR_hostname//[[:blank:]]/}
echo -e "Name for the instance:  ${red}$TF_VAR_hostname${clear}";

echo ""
echo "===================================================================="
TF_VAR_sshkey=$(sed -nr "/^\[INSTANCE\]/ { :l /^sshkey[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_sshkey=${TF_VAR_sshkey//[[:blank:]]/}
echo -e "Name for the sshkey :  ${red}$TF_VAR_sshkey${clear}";

echo ""
echo "===================================================================="
TF_VAR_image=$(sed -nr "/^\[INSTANCE\]/ { :l /^image[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_image=${TF_VAR_image//[[:blank:]]/}
echo -e "Name of the image used for the instance:  ${red}$TF_VAR_image${clear}";

echo ""
echo "===================================================================="
TF_VAR_flavor=$(sed -nr "/^\[INSTANCE\]/ { :l /^flavor[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_flavor=${TF_VAR_flavor//[[:blank:]]/}
echo -e "Flavor used for the instance:  ${red}$TF_VAR_flavor${clear}";
echo "------------------------------------------"
echo "m2.medium     (CPUs=2, RAM=4Gb, disk=20Gb)"
echo -e "m2.large      (CPUs=4, RAM=8Gb, disk=40Gb)-${green}recommended${clear}"
echo "------------------------------------------"

echo ""
echo "===================================================================="
TF_VAR_dnsrecord=$(sed -nr "/^\[CERTIFICATE\]/ { :l /^dnsrecord[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_dnsrecord=${TF_VAR_dnsrecord//[[:blank:]]/}
echo -e "Domain to be used for the certificate:  ${red}$TF_VAR_dnsrecord${clear}";


echo ""
echo "===================================================================="
#export TF_VAR_notebook_image=$(awk -F "=" '/notebook/ {print $2}' config.ini)
DOCKER_NOTEBOOK_IMAGE=$(sed -nr "/^\[JUPYTERHUB\]/ { :l /^notebook[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
DOCKER_NOTEBOOK_IMAGE=${DOCKER_NOTEBOOK_IMAGE//[[:blank:]]/}
replacement=$(sed -nr "/ / { :l /^DOCKER_NOTEBOOK_IMAGE[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./conf/.env)
replacement=${replacement//[[:blank:]]/}
sed -i "s@$replacement@$DOCKER_NOTEBOOK_IMAGE@g" ./conf/.env
echo -e "Docker image used for the notebooks:  ${red}$DOCKER_NOTEBOOK_IMAGE${clear}";


#****************************************** Create the conf file user_data.txt
echo "#cloud-config">>user_data.txt
echo "password: ${TF_VAR_password}">>user_data.txt
echo "chpasswd: { expire: False }">>user_data.txt
echo "ssh_pwauth: True">>user_data.txt


echo ""
echo "===================================================================="
echo "===================================================================="
read -r -p "Do you confirm this configuration? (yes/no) " confirmation


if [ $confirmation == 'yes' ]
then 
 if ! [[ $(terraform -version) ]]; then
	sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
	curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
	sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
	sudo apt-get update && sudo apt-get --yes install terraform
 fi	
 echo "Commencing deployment..."
 terraform 0.13upgrade .
 terraform init
 terraform plan
 terraform apply
fi
