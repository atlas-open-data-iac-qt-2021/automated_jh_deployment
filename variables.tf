variable "username" {
  description = "The username for the DB master user"
  type        = string
}

variable "password" {
  description = "The password for the DB master user"
  type        = string
}

variable "hostname" {
  description = "The name of the VM created in openstack"
  type        = string
}

variable "sshkey" {
  description = "The name of the ssh key created for the instance"
  type        = string
}

variable "dnsrecord" {
  description = "The DNS related to the certificate"
  type        = string
}

variable "image" {
  description = "The image of the VM created in openstack"
  type        = string
}


variable "flavor" {
  description = "The flavor of the VM created in openstack"
  type        = string
}



