  #compute the key to associate to the VM
  resource "openstack_compute_keypair_v2" "test-keypair" {
    name = var.sshkey
  }

  #=====================================================================================================

  #setup of the VM instance considering the specific parameters of openstack cern
  resource "openstack_compute_instance_v2" "test1" { 					
                              
    name            = var.hostname
    image_name      = var.image
    flavor_name     = var.flavor
    key_pair        = "${openstack_compute_keypair_v2.test-keypair.name}"
    security_groups = ["default"]
    #set the server to allow remote access with custom password (psw config performed during the setup)
    user_data       = "${file("user_data.txt")}"   							


  # Copies the config folder to the instance
    provisioner "file" {
      source      = "conf/"
      destination = "."

      #set the type of connection with the user and the psw previoulsy set
      connection {												
        type     = "ssh"
        user     = var.username
        password = var.password
        host     = "${openstack_compute_instance_v2.test1.access_ip_v4}"
      }  
    }

  provisioner "remote-exec" {       									
      inline = [
  #	  "sudo touch /boot/grub/menu.lst",
  #	  "sudo update-grub2",
  #	  "sudo apt update",
  #	  "sudo apt --yes upgrade",
  #	  "sudo apt update",
      "echo ==========================",
      "echo Installing Zerotier",
      "echo ==========================",
      "curl -s https://install.zerotier.com | sudo bash",
      "sudo zerotier-cli join 8286ac0e47bce2df",
      "echo ==========================",
      "echo Installing Docker",
      "echo ==========================",
      "sudo apt --yes install docker.io",
      "echo ==========================",
      "echo Installing Make",
      "echo ==========================",
      "sudo apt --yes install make",
      "echo ==========================",
      "echo Installing docker-compose",
      "echo ==========================",
      "sudo apt --yes install docker-compose",
      "mkdir secrets",
      "echo ==========================",
      "echo Creating cetificate",
      "echo ==========================",
      "sudo openssl req -x509 -newkey rsa:4096 -keyout ./secrets/jupyterhub.key -out ./secrets/jupyterhub.crt -days 365 -nodes -subj \"/CN=${var.dnsrecord}\" ",
      "echo ==========================",
      "echo Make build",
      "echo ==========================",
      "sudo make build",
      "sudo make notebook_image",
      "sudo docker-compose up -d"    			
          ]

    #set the type of connection with the user and the psw previoulsy set
    connection {												
      type     = "ssh"
      user     = var.username
      password = var.password
      host     = "${openstack_compute_instance_v2.test1.access_ip_v4}"
    }  
  }
  }
